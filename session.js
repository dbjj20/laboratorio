const keygen = require('keygen')
const fs = require('fs')
let stream = fs.createWriteStream("my_file.js", {flags: 'a'});

// console.log(test[`8e9bd83cc74`])
function ddd () {
  return keygen.hex(15)
}

let o = 0

function writeToFile() {
  stream.once('open',function(fd) {
    while (true) {
      o = o + 1
      let code = keygen.hex(15)
      stream.write(`{"${code.slice(0,7)}": "${code}"}\n`);
      if (o === 1000 ) {
        break
      }
    }
    stream.end();
  });
}

writeToFile()
