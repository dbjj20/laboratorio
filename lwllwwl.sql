CREATE TABLE Contract
(
    contractId int(11), -- done -> 1
    number varchar(45), -- done -> 2
    code varchar(45), -- done -> 3
    serial int(11),-- done -> 4
    creationDate DATE, -- done -> 19
    expirationDate DATE,
    vigencyYears INT(11), -- done -> 17
    roomTypeId int(11), -- done -> 5
    seasonId int(45), -- done -> 6
    membershipTypeId int(45), -- done -> 7
    qualificationId int(11), -- done -> 8
    statusId int(11),-- done -> 9
    volume DECIMAL(14,6), -- done -> 10
    requiredDownpayment DECIMAL(14,6), -- done -> 13
    requiredDPPercentage DECIMAL(14,6), -- done -> 18
    closingCost DECIMAL(14,6), -- done -> 14
    discount DECIMAL(14,6), -- done -> 11
    paidToday DECIMAL(14,6), -- done -> 15
    paidDownPayment DECIMAL(14,6), -- done -> 12
    financedAmount DECIMAL(14,6), -- done -> 16
    financedPercentage DECIMAL(14,6),
    languageId int(11), -- done -> 20
    payrollId int(11), -- done -> 22
    chargeBack TINYINT(1), -- done 23
    locationId int(11),
    contractLoand int(11),
    maintenanceFeedId int(11),
    readOnly TINYINT(1), -- done -> 24
    weeksUsagePerYear int(5),
    weeksAceleration int(5),
    membershipWeeks int(5), -- done -> 25
    usedMembershipWeeks int(5), -- done -> 26
    expiredMembershipWeeks int(5), -- done -> 27
    preferredRates int(5), -- done
    cancelTypeId int(11), -- done -> 28
    usedPreferredRates int(5), -- done -> 29
    contractTypeId int(11),
    createAt DATE, -- done -> 21
    updateAt DATE,
    createdBy int(11),
    updatedBy int(11)
);
